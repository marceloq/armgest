

package com.bdf.arm.security;

import com.bdf.arm.entity.Account;
import com.bdf.arm.entity.IpWhiteList;
import com.bdf.arm.entity.LoginAudit;
import com.bdf.arm.enums.UserStatusEnum;
import com.bdf.arm.repository.AccountRepository;
import com.bdf.arm.repository.IpWhiteListRepository;
import com.bdf.arm.repository.LoginAuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by leandro on 09/05/16.
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private LoginAuditRepository loginAuditRepository;

    @Autowired
    private IpWhiteListRepository ipWhiteListRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomAuthenticationProvider.class);

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        WebAuthenticationDetails wad = null;
        String userIPAddress = null;

        // Get the IP address of the user tyring to use the site
        wad = (WebAuthenticationDetails) authentication.getDetails();
        userIPAddress = wad.getRemoteAddress();
        List<IpWhiteList> ipList = ipWhiteListRepository.findAllEnabled();

        LOGGER.info("userIPAddress == " + userIPAddress);

        Account account = accountRepository.findByUsername(authentication.getPrincipal().toString());
        String password = authentication.getCredentials().toString();
        //si existe usuario
        if (account != null) {
            if (!account.isEnabled()) {
                persistLoginAudit(account.getId(), account.getUsername(), userIPAddress, UserStatusEnum.FAIL);
                throw new DisabledException("Login Error, el usuario esta deshabilitado");
            }
            //Si es usuario sin restricccion de IPS o si coincide su Ip con la del IPS
            if (account.isWhiteList() && containsIp(ipList, userIPAddress)) {

                if (passwordEncoder.matches(password, account.getPassword().toString())) {
                    Authentication auth = new UsernamePasswordAuthenticationToken(createUser(account), null, account.getAuthorities());
                    persistLoginAudit(account.getId(), account.getUsername(), userIPAddress, UserStatusEnum.SUCCESS);
                    SecurityContextHolder.getContext().setAuthentication(auth);
                    LOGGER.info("Login Satisfactorio con el usuario: " + account.getUsername() + " rol: " + account.getAuthorities() + " ip: " + userIPAddress);
                    return auth;

                } else { //Password incorrecto
                    LOGGER.info("Login Error, Password incorrecto para el usuario: " + authentication.getPrincipal().toString() + " ip: " + userIPAddress);
                    persistLoginAudit(account.getId(), account.getUsername(), userIPAddress, UserStatusEnum.FAIL);
                    throw new BadCredentialsException("Password incorrecto");

                }
            } else { //Ip incorrecta !
                persistLoginAudit(account.getId(), account.getUsername(), userIPAddress, UserStatusEnum.FAIL);
                throw new BadCredentialsException("Su Ip no es correcta");

            }

        } else { //no existe el usuario
            LOGGER.info("Login Error, el usuario no existe : " + authentication.getPrincipal().toString() + " ip: " + userIPAddress);
            persistLoginAudit(null, authentication.getPrincipal().toString(), userIPAddress, UserStatusEnum.FAIL);
//            loginAuditRepository.save(new LoginAudit(null, authentication.getPrincipal().toString(), new Date(), userIPAddress, UserStatusEnum.FAIL));
            throw new UsernameNotFoundException("Login Error, el usuario no existe");
        }
    }


    @Override
    public boolean supports(Class<? extends Object> authentication) {
        // copied it from AbstractUserDetailsAuthenticationProvider
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }

    /**
     * Busqueda de la Ip en la lista de Ip permitidas y habilitadas
     *
     * @param ipList
     * @param userIp
     * @return
     */
    private boolean containsIp(List<IpWhiteList> ipList, String userIp) {
        for (IpWhiteList ip : ipList) {
            if (ip.getIpNumber().equals(userIp)) {
                return true;
            }
        }
        return false;
    }


    private User createUser(Account account) {
        return new User(account);
    }

    private static class User extends org.springframework.security.core.userdetails.User {

        private final Account account;

        public User(Account account) {
            super(account.getUsername(), account.getPassword(), account.getAuthorities());
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public boolean isAdmin() {
            return getAccount().isAdmin();
        }
    }

    /**
     * Persisitr en la tabla de auditoria de Login
     *
     * @param id
     * @param userName
     * @param userIp
     * @param status
     */
    private void persistLoginAudit(Long id, String userName, String userIp, UserStatusEnum status) {

        LoginAudit loginAudit = new LoginAudit();
        loginAudit.setCreationTime(new Date());
        loginAudit.setIp(userIp);
        loginAudit.setStatus(status);
        loginAudit.setUserId(id);
        loginAudit.setUserName(userName);

        loginAuditRepository.save(loginAudit);

    }


}