package com.bdf.arm.entity;

import javax.persistence.*;

/**
 * Created by leandro on 09/05/16.
 */
@Entity
@Table(name = "ip_white_list")
@NamedQuery(name = IpWhiteList.FIND_ALL_ENABLED, query = "select a from IpWhiteList a where a.isEnabled = true")
public class IpWhiteList extends BaseEntityFull {

    public static final String FIND_ALL_ENABLED = "IpWhiteList.findAllEnabled";

    @Column(name = "ip_number")
    private String ipNumber;

    private String description;

    @Column(name = "enabled")
    private Boolean isEnabled;

    public IpWhiteList(String ipNumber, String description, Boolean isEnabled) {
        this.ipNumber = ipNumber;
        this.description = description;
        this.isEnabled = isEnabled;
    }

    public IpWhiteList() {
    }

    public String getIpNumber() {
        return ipNumber;
    }

    public void setIpNumber(String ipNumber) {
        this.ipNumber = ipNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }


}


