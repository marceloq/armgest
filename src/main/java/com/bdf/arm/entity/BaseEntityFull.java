package com.bdf.arm.entity;

import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * Created by leandro on 23/05/16.
 */
@MappedSuperclass
public class BaseEntityFull extends BaseEntity {

    private Date modificationTime;

    public Date getModificationTime() {
        return modificationTime;
    }

    public void setModificationTime(Date modificationTime) {
        this.modificationTime = modificationTime;
    }
}