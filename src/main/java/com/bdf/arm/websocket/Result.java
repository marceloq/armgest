package com.bdf.arm.websocket;

/**
 * Created by leandro on 18/05/16.
 */
public class Result {
    private String result;

    public Result(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}