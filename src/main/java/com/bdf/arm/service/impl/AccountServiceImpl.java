package com.bdf.arm.service.impl;

import com.bdf.arm.entity.Account;
import com.bdf.arm.repository.AccountRepository;
import com.bdf.arm.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by leandro on 26/05/16.
 */
@Component
public class AccountServiceImpl implements AccountService {


    @Autowired
    private AccountRepository accountRepository;


    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();

    }

    @Override
    public Account save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public Account findByUsername(String userName) {
        return accountRepository.findByUsername(userName);
    }


}
