package com.bdf.arm.service;

import com.bdf.arm.entity.UserRole;

import java.util.List;

/**
 * Created by leandro on 26/05/16.
 */
public interface UserRoleService {

    List<UserRole> findAll();

}
