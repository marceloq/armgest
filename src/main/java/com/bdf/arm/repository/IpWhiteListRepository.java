package com.bdf.arm.repository;

import com.bdf.arm.entity.IpWhiteList;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by leandro on 11/05/16.
 */
public interface IpWhiteListRepository extends CrudRepository<IpWhiteList, Long> {

    List<IpWhiteList> findAllEnabled();

}


