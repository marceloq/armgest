package com.bdf.arm.repository;

import com.bdf.arm.entity.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by leandro on 26/05/16.
 */
public interface UserRoleRepository extends CrudRepository<UserRole, Long> {

    List<UserRole> findAll();
}
