package com.bdf.arm.repository;

import com.bdf.arm.entity.LoginAudit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by leandro on 05/05/16.
 */

@Repository
@Transactional(readOnly = true)
public class LoginAuditRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginAuditRepository.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void save(LoginAudit loginAudit) {
        entityManager.persist(loginAudit);
    }
}
